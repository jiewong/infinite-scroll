const imageContainer = document.getElementById('image-container');
const loader = document.getElementById('loader');
// Unsplasd API
let count = 5;
const apiKey = `yFsQUnHFB3MYdGs-OaK9YCpGF6taGLLXV3a2--wTvHw`;
let apiUrl = `https://api.unsplash.com/photos/random/?client_id=${apiKey}&count=${count}`;

let ready = false;
let imagesLoaded = 0;
let totalImages = 0;
let photosArray = [];

//check if all image were loaded
function imageLoaded() {
    imagesLoaded++;
    console.log(imagesLoaded);
    if(imagesLoaded === totalImages){
        ready = true;
        loader.hidden = true;
        console.log('ready = ', ready)

        count = 30;
    }
}

            //Helper Function to set Attribute on DOM Elements
                function setAttributes(element, attributes) {
                    for (const key in attributes) {
                        element.setAttribute(key, attributes[key]);
                    }
                }
//Create Elements for links & photos, Add to DOM
function displayPhotos(){
    imagesLoaded = 0;
    totalImages = photosArray.length;
    console.log('total images', totalImages)
//Run Function forEach
    photosArray.forEach((photo) => {
        //Create <a> to link to unsplash
        const item = document.createElement('a');
        // item.setAttribute('href', photo.links.html);
        // item.setAttribute('target', '_blank');
                setAttributes(item, {       //Helper Function 
                    href: photo.links.html, //Helper Function 
                    target: '_blank',       //Helper Function 
                });                         //Helper Function 
        //Create <img> for photo
        const img = document.createElement('img');
        // img.setAttribute('src', photo.urls.regular);
        // img.setAttribute('alt', photo.alt_description);
        // img.setAttribute('title', photo.alt_description);
                setAttributes(img, {               //Helper Function 
                    src: photo.urls.regular,         //Helper Function 
                    alt: photo.alt_description,     //Helper Function 
                    title: photo.alt_description,   //Helper Function 
                });                                 //Helper Function 
        
        //Event Listener, check when each is finished loading
        img.addEventListener('load', imageLoaded);
        //put <img> inside <a>, then put both inside imageContainer element
        item.appendChild(img);
        imageContainer.appendChild(item);
    });
}

//Get photos from Unsplash API
async function getPhotos() {
    try {
        const response = await fetch(apiUrl);
         photosArray = await response.json();
        displayPhotos();
    } catch (error){

    }
}

// Check to see if scrolling near bottom of page, load more photos
window.addEventListener('scroll', () => {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight - 1000 && ready){
        ready = false;
        getPhotos();
      
    }
})

//Onload
getPhotos();